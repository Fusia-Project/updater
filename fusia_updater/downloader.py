from tempfile import NamedTemporaryFile, TemporaryDirectory

from urllib3.util import parse_url


def download(pkg_url, filename=None):
    url = parse_url(pkg_url)
    if url.scheme is None:
        print("Couldn't resolve scheme. Assuming HTTP.")
        pkg_url = "http://" + pkg_url
        return _http_download(pkg_url, filename)
    if url.path.endswith(".git"):
        return _git_download(pkg_url, filename)
    if url.scheme == "http":
        return _http_download(pkg_url, filename)
    if url.scheme == "https":
        return _http_download(pkg_url, filename)
    if url.scheme == "ssh":
        return _scp_download(pkg_url, filename)
    
    raise NotImplementedError(f"Unable to recognize {pkg_url}")


def _http_download(url, filename):
    import requests
    download = requests.get(url, stream=True)
    with NamedTemporaryFile(delete=False, prefix=filename) as tmpfile:
        for block in download.iter_content():
            tmpfile.write(block)
        return tmpfile.name


def _git_download(url, filename):
    import git
    from zipfile import ZipFile
    from tempfile import mktemp
    from os import walk
    from os.path import join as pjoin
    with TemporaryDirectory() as repodir:
        repo = git.Repo()
        repo.clone_from(url, repodir)
        if filename is None:
            filename = ""
        pkg_name = mktemp(prefix=filename)
        with ZipFile(pkg_name, mode="w") as pkg:
            for root, dirs, files in walk(repodir):
                for fn in files:
                    pkg.write(pjoin(root, fn))
    return pkg_name


def _scp_download(url, filename):
    raise NotImplementedError("Downloading from SSH not yet implemented.")
