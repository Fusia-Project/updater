from python:3.7

workdir /opt/freezr
run pip install poetry

volume /opt/freezr

cmd poetry install && poetry add pip && poetry run pip freeze > requirements.txt
