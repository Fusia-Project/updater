from random import randrange
from re import escape, search
from string import ascii_letters
from pytest import raises, mark

from fusia_updater.downloader import download


def test_http():
    prefix = ""
    for i in range(5):
        prefix = prefix + ascii_letters[randrange(0, len(ascii_letters))]
    regex = escape(prefix)+r".*"
    assert search(regex, download("https://gitlab.com/OnDev-project/Fusia/raw/master/fusia.spec", prefix)) is not None

@mark.xfail
def test_git_ssh():
    prefix = ""
    for i in range(5):
        prefix = prefix + ascii_letters[randrange(0, len(ascii_letters))]
    regex = escape(prefix)+r".*"
    assert search(regex, download("ssh://git@gitlab.com/OnDev-project/Fusia.git", prefix)) is not None


def test_git_https():
    prefix = ""
    for i in range(5):
        prefix = prefix + ascii_letters[randrange(0, len(ascii_letters))]
    regex = escape(prefix)+r".*"
    assert search(regex, download("https://gitlab.com/OnDev-project/Fusia.git", prefix)) is not None

@mark.xfail
def test_scp():
    prefix = ""
    for i in range(5):
        prefix = prefix + ascii_letters[randrange(0, len(ascii_letters))]
    regex = escape(prefix)+r".*"
    assert search(regex, download("ssh://gitlab.com/OnDev-project/Fusia", prefix)) is not None
